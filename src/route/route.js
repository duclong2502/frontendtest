import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import login from '../screen/login';
import register from '../screen/register';

class MainRoute extends Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path='/login' component={login} />
                    <Route exact path='/register' component={register} />
                </Switch>
            </BrowserRouter>
        );
    }
}

export default MainRoute;
