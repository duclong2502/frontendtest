import React from 'react';
import './css/style.css';
import './App.css';
import MainRoute from './route/route'

function App() {
  return (
    <div className="App">
      {/* {{login}} */}
      <MainRoute />
    </div>
  );
}

export default App;
