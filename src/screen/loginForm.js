import React, { Component } from 'react';
import { Form, Icon, Input, Button, Row, Tabs, Col } from 'antd';

class loginForm extends Component {
    constructor(props) {
        super(props);
        
    }
    handleSubmitLogin = event => {
        event.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('.....Login: ', values);
            }
        });
    };
    
    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Row>
                <h1 style={{ color: 'white' }}>Welcome Back</h1>
                <Form onSubmit={this.handleSubmitLogin} className='login-form'>
                    <Form.Item>
                        {getFieldDecorator('email', {
                            rules: [
                                {
                                    type: 'email',
                                    message: 'The input is not valid E-mail!'
                                },
                                {
                                    required: true,
                                    message: 'Please input your E-mail!'
                                }
                            ]
                        })(<Input placeholder='Email Address*' />)}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('password', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Please input your Password!'
                                }
                            ]
                        })(
                            <Input
                                style={{ color: 'red' }}
                                type='password'
                                placeholder='Password'
                            />
                        )}
                    </Form.Item>
                    <Form.Item>
                        <a className='login-form-forgot' href=''>
                            Forgot password
                        </a>
                        <Button htmlType='submit' className='login-form-button'>
                            LOG IN
                        </Button>
                    </Form.Item>
                </Form>
            </Row>
        );
    }
}

export default Form.create()(loginForm);
