import React, { Component } from 'react';
import { Form, Icon, Input, Button, Row, Tabs, Col } from 'antd';
import RegisterForm from './register';
import LoginForm from './loginForm';

const { TabPane } = Tabs;

class login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            tab: ''
        };
    }

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
        // console.log(event.target.value);
    };

    callback = key => {
        console.log(key);
    };
    render() {
        return (
            <div style={{height:'600px'}}>
                <div className='form-login'>
                    {/* {registerForm} */}
                    <Tabs
                        defaultActiveKey='1'
                        onChange={this.callback}
                        className='tab'
                    >
                        <TabPane tab='Log In' key='1' style={{ width: '100%' }}>
                            <LoginForm />
                        </TabPane>
                        <TabPane tab='Sign Up' key='2'>
                            <RegisterForm />
                        </TabPane>
                    </Tabs>
                </div>
            </div>
        );
    }
}

export default Form.create()(login);
