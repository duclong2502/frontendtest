import React, { Component } from 'react';
import { Form, Icon, Input, Button, Row, Tabs, Col } from 'antd';

class register extends Component {
    constructor(props) {
        super(props);
    }
    handleSubmitRegister = event => {
        event.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('........sign up: ', values);
            }
        });
    };
    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <div>
                <Row>
                    <h1 style={{ color: 'white' }}>Sign Up For Free</h1>
                    <Form
                        onSubmit={this.handleSubmitRegister}
                        className='login-form'
                    >
                        <Row gutter={16}>
                            <Col span={12}>
                                <Form.Item>
                                    {getFieldDecorator('firstname', {
                                        rules: [
                                            {
                                                required: true,
                                                message:
                                                    'Please input your username!'
                                            }
                                        ]
                                    })(
                                        <Input
                                            prefix={
                                                <Icon
                                                    type='user'
                                                    style={{
                                                        color: 'rgba(0,0,0,.25)'
                                                    }}
                                                />
                                            }
                                            placeholder='First Name*'
                                        />
                                    )}
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item>
                                    {getFieldDecorator('lastname', {
                                        rules: [
                                            {
                                                required: true,
                                                message:
                                                    'Please input your username!'
                                            }
                                        ]
                                    })(
                                        <Input
                                            prefix={
                                                <Icon
                                                    type='user'
                                                    style={{
                                                        color: 'rgba(0,0,0,.25)'
                                                    }}
                                                />
                                            }
                                            placeholder='Last Name*'
                                        />
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Form.Item>
                            {getFieldDecorator('email', {
                                rules: [
                                    {
                                        type: 'email',
                                        message:
                                            'The input is not valid E-mail!'
                                    },
                                    {
                                        required: true,
                                        message: 'Please input your E-mail!'
                                    }
                                ]
                            })(<Input placeholder='Email Address*' />)}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('password', {
                                rules: [
                                    {
                                        required: true,
                                        message: 'Please input your Password!'
                                    }
                                ]
                            })(
                                <Input
                                    style={{ color: 'red' }}
                                    type='password'
                                    placeholder='Set A Password*'
                                />
                            )}
                        </Form.Item>
                        <Form.Item>
                            <Button
                                htmlType='submit'
                                className='login-form-button'
                            >
                                GET STARTED
                            </Button>
                        </Form.Item>
                    </Form>
                </Row>
            </div>
        );
    }
}

export default Form.create()(register);
